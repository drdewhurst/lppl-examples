##
# \file
# This file is part of lppl-examples.
# Copyright David Rushing Dewhurst, 2022 - present.
# Released under GPLv3
#

import logging
import sys

import pandas as pd
import matplotlib.pyplot as plt


def main():
    try:
        df1 = pd.read_csv("../out/dynamic-results-diy.csv", sep="\t")
        df2 = pd.read_csv("../out/dynamic-results-builtin.csv", sep="\t")
    except FileNotFoundError:
        logging.error("One or more files not found -- have you run ./dynamic yet?")
        sys.exit()
    
    fig, axes = plt.subplots(2, 1)
    for a in axes:
        a.grid("on")

    axes[0].plot(df1.index, df1.observed, marker="o", label="ground truth", alpha=0.5, color='k')
    axes[0].plot(df1.index, df1.latent, marker="o", label="inferred state (diy)", alpha=0.5, color='cyan')
    axes[1].plot(df1.index, df1.log_latent_scale, marker="o", label="log uncertainty (diy)", alpha=0.5, color='cyan')

    axes[0].plot(df2.index, df2.latent, marker="o", label="inferred state (builtin)", alpha=0.5, color='crimson')
    axes[1].plot(df2.index, df2.log_latent_scale, marker="o", label="log uncertainty (builtin)", alpha=0.5, color='crimson')

    axes[0].set_xticklabels([])
    axes[1].set_xlabel("timestep")
    for a in axes:
        a.legend()
    fig.savefig("../out/plot-dynamic-results.png")
    plt.close()


if __name__ == "__main__":
    main()
