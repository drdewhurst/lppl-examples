/** 
 * \file
 * This file is part of lppl-examples.
 * Copyright David Rushing Dewhurst, 2022 - present.
 * Released under GPLv3
 */

#include <array>
#include <cmath>
#include <functional>
#include <tuple>

#include <distributions.hpp>
#include <record.hpp>

#include <update/particle.hpp>
#include <inference/inference.hpp>
#include <inference/proposal.hpp>
#include <inference/importance/generic.hpp>
#include <query.hpp>

std::minstd_rand rng(20220921);

// don't use shared_ptr to array, but rather shared_ptr to multiple contiguous arrays

template<size_t N>
struct data_1d {
    std::array<double, N> x;
    std::array<double, N> y;
};

using rec = record_t<DTypes<Normal, Gamma>>;

/**
 * @brief A basic linear regression model
 * 
 * @tparam N observation dimensionality (size of data set)
 * @param r
 * @param data 
 * @return std::tuple<double, double> (intercept, slope)
 */
template<size_t N>
std::tuple<double, double>
linear_regression(
    rec& r,
    std::shared_ptr<data_1d<N>> data
) {
    auto d = *data;
    auto intercept = sample(r, "intercept", Normal(), rng);
    auto slope = sample(r, "slope", Normal(), rng);
    auto scale = sample(r, "scale", Gamma(), rng);
    for (size_t ix = 0; ix != N; ix++) {
        observe(
            r,
            "obs/" + std::to_string(ix),
            Normal(d.x[ix] * slope + intercept, scale),
            d.y[ix]
        );
    }
    return std::make_tuple(intercept, slope);
}

/**
 * @brief Naive proposal with uninformed wide distributions and an arbitrary upper cutoff
 *  on the scale parameter
 * 
 */
template<size_t N>
struct linreg_proposal_1 {

    linreg_proposal_1() = default;

    double operator()(rec& r, std::shared_ptr<data_1d<N>>) {
        sample(r, "intercept", Normal(0.0, 10.0), rng);
        sample(r, "slope", Normal(0.0, 10.0), rng);
        propose<Gamma>(r, "scale", Uniform(2.0), rng);
        return loglatent(r);
    }
};

template<size_t N>
double mean(std::array<double, N> * arr) {
    double out;
    auto v = *arr;
    for (size_t n = 0; n != N; n++) {
        out += v[n];
    }
    return out / N;
}

/**
 * @brief A less-naive proposal that uses the MLE linear regression parameters
 *  to center the distributions on slope and intercept
 * 
 * @tparam N the dimensionality of the data set
 */
template<size_t N>
struct linreg_proposal_2 {

    double est_slope;
    double est_intercept;

    linreg_proposal_2(std::shared_ptr<data_1d<N>> data) {
        auto d = *data;
        double mean_x = 0.0, mean_y = 0.0, num = 0.0, denom = 0.0;
        mean_x = mean(&(d.x));
        mean_y = mean(&(d.y));
        for (size_t n = 0; n != N; n++) {
            num += (d.x[n] - mean_x) * (d.y[n] - mean_y);
            denom += std::pow(d.x[n] - mean_x, 2.0);
        }
        est_slope = num / denom;
        est_intercept = mean_y - est_slope * mean_x;
        std::cout << "MLE slope = " << est_slope << std::endl;
        std::cout << "MLE intercept = " << est_intercept << std::endl;
    }

    double operator()(rec& r, std::shared_ptr<data_1d<N>>) {
        sample(r, "intercept", Normal(est_slope, 2.0), rng);
        sample(r, "slope", Normal(est_intercept, 2.0), rng);
        sample(r, "scale", Gamma(), rng);
        return loglatent(r);
    }
};

int main() {
    // true parameters
    constexpr size_t dim = 22;
    double true_intercept = 1.0, true_slope = -0.75, true_scale = 1.3;

    // random exogenous variable and noise
    auto x_dist = Normal(3.0, 5.0);
    auto noise_dist = Normal(0.0, true_scale);

    // fake dataset
    auto d = std::make_shared<data_1d<dim>>();
    for (size_t n = 0; n != dim; n++) {
        d->x[n] = x_dist.sample(rng);
        d->y[n] = d->x[n] * true_slope + true_intercept + noise_dist.sample(rng);
    }

    // inference with the first proposal
    std::cout << "~~~ inference with proposal 1 (naive, broad) ~~~" << std::endl;
    pp_t<std::shared_ptr<data_1d<dim>>, std::tuple<double, double>, Normal, Gamma> f = linear_regression<dim>;
    auto q = weighted_record<std::tuple<double, double>, Normal, Gamma>();
    exog_proposal_t<std::shared_ptr<data_1d<dim>>, Normal, Gamma> prop1 = linreg_proposal_1<dim>();
    auto opts = inf_options_t(10000);
    auto infer1 = inference<ImportanceSampling>(f, *q, opts);
    auto posterior1 = infer1(d, prop1);
    std::cout << "Posterior means with proposal 1 = " << std::endl;
    for (const auto& s : {"intercept", "slope", "scale"}) {
        std::cout << s << " = " << mean(posterior1, s) << std::endl;
    }

    // inference with the second proposal
    // note slightly better sharpness with 1/5 samples -- proposal design is worth it
    std::cout << std::endl;
    std::cout << "~~~ inference with proposal 2 (starting with MLE) ~~~" << std::endl;
    q->clear();
    exog_proposal_t<std::shared_ptr<data_1d<dim>>, Normal, Gamma> prop2 = linreg_proposal_2(d);
    opts = inf_options_t(2000);
    auto infer2 = inference<ImportanceSampling>(f, *q, opts);
    auto posterior2 = infer2(d, prop2);
    std::cout << "Posterior means with proposal 2 = " << std::endl;
    for (const auto& s : {"intercept", "slope", "scale"}) {
        std::cout << s << " = " << mean(posterior2, s) << std::endl;
    }

    return 0;
}
