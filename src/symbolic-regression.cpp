/**
 * \file 
 * This file is part of lppl-examples.
 * Copyright David Rushing Dewhurst, 2022 - present.
 * Released under GPLv3
 */

#include <array>
#include <chrono>
#include <cmath>
#include <functional>
#include <iostream>
#include <limits>
#include <numeric>
#include <string>

#include <distributions.hpp>
#include <record.hpp>
#include <inference/metropolis/base.hpp>
#include <query.hpp>

std::minstd_rand rng(20220921);

// display utilities

template<typename T>
struct type_string {
    static inline std::string value = typeid(T).name();
};
template<>
struct type_string<double> {
    static inline std::string value = "double";
};
template<>
struct type_string<bool> {
    static inline std::string value = "bool";
};

// basic operations in the language
// these are pure functions

template<class T = void>
struct max {
    constexpr T operator()(const T& lhs, const T& rhs) const {
        return lhs > rhs ? lhs : rhs;
    }
};

template<typename T, size_t N>
struct max_op {
    T operator()(const std::array<T, N>& x) const {
        return std::accumulate(
            x.begin(), x.end(), -std::numeric_limits<T>::infinity(), max<T>()
        );
    }
    std::string repr() { return "max"; }
};

template<typename T, size_t N>
struct sum_op {
    T operator()(const std::array<T, N>& x) const {
        return std::accumulate(
            x.begin(), x.end(), 0, std::plus<T>()
        );
    }
    std::string repr() { return "sum"; }
};

template<typename T, size_t N>
struct mean_op {
    T operator()(const std::array<T, N>& x) const {
        return sum_op<T, N>()(x) / x.size(); 
    }
    std::string repr() { return "mean"; }
};

template<typename T, size_t N>
struct std_op {
    T operator()(const std::array<T, N>& x) const {
        auto loc = mean_op<T, N>()(x);
        T m2;
        for (auto& value : x) {
            m2 += std::pow(value - loc, 2.0);
        }
        return std::sqrt(m2 / N);
    }
    std::string repr() { return "std"; }
};

template<typename T, size_t N>
struct normalize_trans {
    std::array<T, N> operator()(const std::array<T, N>& x) const {
        std::array<T, N> out;
        auto loc = mean_op<T, N>()(x);
        auto scale = std_op<T, N>()(x);
        for (size_t ix = 0; ix != N; ix++) {
            out[ix] = (x[ix] - loc) / scale;
        }
        return out;
    }
    std::string repr() { return "normalize"; }
};

template<typename T, size_t N>
struct abs_trans {
    std::array<T, N> operator()(const std::array<T, N>& x) const {
        std::array<T, N> out;
        for (size_t ix = 0; ix != N; ix++) {
            out[ix] = x[ix] > 0 ? x[ix] : -x[ix];
        }
        return out;
    }
    std::string repr() { return "abs"; }
};

// this, too, is a pure function -- it defines a dependent type in
// the language

template<typename T, size_t N>
struct threshold_b {
    T _t;
    threshold_b(T t) : _t(t) {}
    std::array<bool, N> operator()(const std::array<T, N>& x) const {
        std::array<bool, N> out;
        for (size_t ix = 0; ix != N; ix++) {
            out[ix] = x[ix] > _t ? true : false;
        }
        return out;
    }
    std::string repr() { return "threshold[" + std::to_string(_t) + "]"; }
};

template<typename T, size_t N>
struct identity_trans {
    std::array<T, N> operator()(const std::array<T, N>& x) const { return x; }
};

template<typename T, size_t N>
std::string display(const std::array<T, N> v) {
    std::string s;
    for (auto& value : v) s += std::to_string(value) + " ";
    return s;
}

template<size_t N>
using trans_t = std::variant<
    normalize_trans<double, N>, 
    abs_trans<double, N>,
    identity_trans<double, N>
>;

template<size_t N>
using b_t = std::variant<
    threshold_b<double, N>
>;

struct score_t {
    double _weight;
    bool _pred;

    score_t() : _weight(0.0), _pred(true) {}
    score_t(bool pred) : _weight(0.0), _pred(true) {}
    score_t(bool pred, double weight) : _weight(weight), _pred(pred) {}

    template<typename RNG>
    bool sample(RNG& rng) { return _pred; }

    double logprob(bool obs) const { return obs == _pred ? 0.0 : _weight; }

    std::string string() const { 
        return "score_t(weight=" 
            + std::to_string(_weight) + ", pred=" 
            + std::to_string(_pred) + ")"; 
    }
};

template<size_t N>
trans_t<N> select_trans(record_t<DTypes<Categorical, score_t>>& r, size_t ix){
    auto x = sample(r, "trans" + std::to_string(ix), Categorical(3), rng);
    if (x == 0) {
        return normalize_trans<double, N>();
    } else if (x == 1) {
        return abs_trans<double, N>();
    } else {
        return identity_trans<double, N>();
    }
}

template<size_t N>
b_t<N> select_b(record_t<DTypes<Categorical, score_t>>& r, size_t ix){
    auto x = sample(r, "b" + std::to_string(ix), Categorical(3), rng);
    if (x == 0) {
        return b_t<N>(threshold_b<double, N>(1.0));
    } else if (x == 1) {
        return b_t<N>(threshold_b<double, N>(2.0));
    } else {
        return b_t<N>(threshold_b<double, N>(3.0));
    }
}

template<size_t N>
using prog_t = std::vector<std::variant<trans_t<N>, b_t<N>>>;

template<size_t N>
struct data {
    std::array<double, N>& x;
    std::array<bool, N>& y;
};

template<size_t N>
std::array<bool, N> evaluate(prog_t<N>& program, std::array<double, N> x) {
    size_t prog_size = program.size();
    for (size_t ix = 0; ix != prog_size - 1; ix++) {
        x = std::visit(
            [x](const auto& f) -> std::array<double, N> { return f(x); },
            std::get<trans_t<N>>(program[ix])
        );
    }
    return std::visit(
        [x](const auto& f) -> std::array<bool, N> { return f(x); },
        std::get<b_t<N>>(program[prog_size - 1])
    );
}

template<size_t N>
prog_t<N> stochastic_opt_filter(record_t<DTypes<Categorical, score_t>>& r, data<N> d) {
    prog_t<N> prog; 
    size_t ix = 0;
    do {
        prog.push_back(select_trans<N>(r, ix));
        ix += 1;
    } while (sample(r, "stop after" + std::to_string(ix - 1), Categorical(), rng) < 1);
    prog.push_back(select_b<N>(r, ix));
    std::array<bool, N> y_pred = evaluate(prog, d.x);
    for (size_t ix = 0; ix != N; ix++) {
        observe(r, "obs" + std::to_string(ix), score_t(y_pred[ix], std::log(0.1)), d.y[ix]);
    }
    return prog;
}

// DEMOS //
// our time series are each of length 7 (because why not?)
// for variable length ts, use std::vector in all of the above
// instead, or pad lengths to constexpr length
constexpr size_t len_ts = 7;

int demo_ops() {
    std::array<double, len_ts> x_data = {-1.0, 0.0, 1.0, 2.0, 3.0, 12.0, 5.0};
    std::array<bool, len_ts> y_data = {false, false, false, true, false, true, true};
    data<len_ts> d = {x_data, y_data};

    std::cout << "~~~ example operations ~~~" << std::endl;
    std::cout << "Original vector: " << display(d.x) << std::endl;
    std::cout << "Max value: " << max_op<double, len_ts>()(d.x) << std::endl;
    std::cout << "Sum values: " << sum_op<double, len_ts>()(d.x) << std::endl;
    auto norm_x = normalize_trans<double, len_ts>()(d.x);
    std::cout << "Normalized vector: " << display(norm_x) << std::endl;
    auto thresh_x = threshold_b<double, len_ts>(2.0)(norm_x);
    std::cout << "Thresholded vector: " << display(thresh_x) << std::endl << std::endl;

    std::cout << "~~~ sampling over programs: stochastic optimization ~~~" << std::endl;
    std::cout << "~ here's one sample from the distribution ~" << std::endl;
    record_t<DTypes<Categorical, score_t>> r;
    auto prog = stochastic_opt_filter(r, d);
    std::cout << display(r) << std::endl;
    // time it!
    auto start_time = std::chrono::high_resolution_clock::now();
    evaluate(prog, d.x);
    auto elapsed = std::chrono::high_resolution_clock::now() - start_time;
    auto tt = std::chrono::duration_cast<std::chrono::microseconds>(elapsed).count();
    std::cout << "~ evaluation of constructed program took " << tt << "us ~" << std::endl;

    std::cout << "~ here's using MCMC to sample from the posterior (curtailed for brevity) ~" << std::endl;
    // This is equivalent to strongly-typed simulated annealing over the space of programs
    // expressed in this DSL -- run for more iterations/use different queryers to see more
    // refined posterior distributions -- the MAP point of the posterior is what you'd get 
    // from a simulated annealing implementation
    auto record_q = weighted_record<prog_t<len_ts>, Categorical, score_t>();
    pp_t<data<len_ts>, prog_t<len_ts>, Categorical, score_t> f = stochastic_opt_filter<len_ts>;
    // options for inference
    size_t thin = 25;
    size_t burn = 1000;
    size_t nsamp = 25;
    auto opts = inf_options_t(thin, burn, burn + thin * nsamp);
    auto infer = inference<AncestorMetropolis>(f, *record_q, opts);
    auto some_records = infer(d);
    auto r_samp = some_records->sample(rng);
    std::cout << "sampled program from posterior:\n" << display(*r_samp) << std::endl;

    return 0;
}

int main() {
    demo_ops();

    return 0;
}