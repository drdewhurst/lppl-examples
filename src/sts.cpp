/**  
 * @file
 * This file is part of lppl-examples.
 * Copyright David Rushing Dewhurst, 2022 - present.
 * Released under GPLv3
 */

#include <cmath>
#include <iostream>
#include <optional>
#include <string>
#include <type_traits>
#include <vector>

#include <distribution_traits.hpp>
#include <distributions.hpp>
#include <effects.hpp>
#include <record.hpp>

#include <inference/inference.hpp>
#include <inference/metropolis/base.hpp>


std::minstd_rand rng(2022);


namespace sts {

    struct data_t { 
        std::vector<std::optional<double>>& data;
        size_t start_ix;

        data_t(std::vector<std::optional<double>>& data)
            : data(data), start_ix(0) {}
    };

    double local_level(
        record_t<DTypes<Normal, Gamma>>& r,
        std::shared_ptr<data_t> data
    ) {
        // prior stuff
        auto loc = sample(r, "ll/loc", Normal(0.0, 3.0), rng);
        auto prec = std::sqrt(sample(r, "ll/prec", Gamma(2.0, 2.0), rng));
        auto prev_level = sample(r, "ll/level/ic", Normal(0.0, 100.0), rng);
        double level;

        size_t ix = data->start_ix, sz = data->data.size();
        for (auto& value : data->data) {
            level = sample(
                r,
                "ll/level/" + std::to_string(ix),
                Normal(loc + prev_level, 1.0 / prec),
                rng
            );
            if (value.has_value()) {
                auto obs = observe(
                    r,
                    "ll/obs/" + std::to_string(ix),
                    Normal(level, 0.5),
                    value.value()
                );
            } else {
                auto obs = sample(
                    r,
                    "ll/obs/" + std::to_string(ix),
                    Normal(level, 0.5),
                    rng
                );
            }
            ++ix;
            prev_level = level;
        }
        return level;
    }
}


int main(int argc, char ** argv) {
    std::vector<std::optional<double>> time_series = {
        100.0,
        100.5,
        100.6,
        std::nullopt,
        std::nullopt,
        99.5,
        std::nullopt,
        101.3,
        std::nullopt,
        std::nullopt,
        102.0,
        std::nullopt,
        std::nullopt,
        std::nullopt,
        102.1,
        102.15, 102.3, 102.25, 102.2,
        104.0, std::nullopt, 104.1
    };

    // what's needed for inference and querying
    pp_t<std::shared_ptr<sts::data_t>, double, Normal, Gamma> ll_f = sts::local_level;
    auto data = std::make_shared<sts::data_t>(time_series);
    auto q = weighted_record<double, Normal, Gamma>();

    // sampler options
    size_t thin = 100;
    size_t burn = 5000;
    size_t nsamp = 100;
    auto opts = inf_options_t(thin, burn, burn + thin * nsamp);

    // do it!
    auto infer = inference<AncestorMetropolis>(ll_f, *q, opts);
    auto out = infer(data);
    auto r_samp = out->sample(rng);
    std::cout << "Sampled posterior record:\n" << display(*r_samp) << std::endl;

    return 0;
}
