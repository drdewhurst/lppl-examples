# `lppl-examples`

Some examples of using [`lppl`](https://davidrushingdewhurst.com/lppl/) for a variety of tasks. Updated very interGPLv3tently.

## Requirements and installation

To build and run the examples, you need CMake 3.20.0 or newer, a C++20 compiler, and `lppl` v0.9.0 or newer. You can get `lppl` by `cd include && git clone -b master git@gitlab.com:drdewhurst/lppl.git`. To generate the plots, you need python 3.9 with pandas and matplotlib installed. Create a new conda environment and use the `requirements.txt` if in doubt.

+ Building the examples: `cd build && cmake .. && make`
+ Running the examples: `cd build && ./the_executable`
+ Plotting the examples: `cd src && my/python/install the_plot_script`

## Examples

+ `dynamic.cpp`: filtering algorithm comparison -- rolling your own time series filtering using queryers and importance sampling vs. using builtin generic filtering algorithms
+ `linear-regression.cpp`: linear regression, fast and slow (with worse and better user-defined proposal distributions)
+ `sts.cpp`: *WIP* basic structural time series models in discrete time (for now -- continuous time later)
+ `symbolic-regression.cpp`: symbolic regression over a pure-functional DSL, featuring the simplest interpreter ever


## License etc.

lppl-examples is licensed under the GPLv3 license. Enjoy! Copyright David Rushing Dewhurst, 2022 - present.